import yaml

class Config:
    def __init__(self, config_file):
        config        = self.get_config(config_file)
        self.hostname = str(config['connection']['hostname'])
        self.port     = int(config['connection']['port'])
        self.ssl      = bool(config['connection']['ssl'])
        self.nick     = config['bot']['nick']
        self.ident    = config['bot']['ident']
        if config['bot']['nickserv_pass']:
            self.nickserv_pass = config['bot']['nickserv_pass']
        else:
            self.nickserv_pass = False

    def get_config(self, config_file):
        try:
            with open(config_file, 'r') as config:
                return yaml.load(config)

        except:
            raise("Unable to open config file")


def test_Config():
    config = Config("../example_config.yaml")
    assert config.hostname == 'irc.darkscience.net'
    assert config.ssl == True
    assert config.nick == 'BottyMcFace'
    assert config.port == int(6697)
