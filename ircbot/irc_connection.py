import socket
import ssl
import asyncio
import logging

class IRCConnection(asyncio.Protocol):
    def __init__(self, loop):
        self.loop = loop

    def connection_create(self, transport):
        pass

    def connection_made(self, transport):
        pass

    def data_received(self, data):
        print('Data received: {!r}'.format(data.decode()))

    def connection_lost(self, exc):
        print('The server closed the connection')
        self.loop.stop()
